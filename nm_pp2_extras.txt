
 * Extra P&P2 questions

This is an archive of extra P&P2 questions that can be used by AMs if they feel
they should investigate a bit more.

The idea is that it should be obvious to see whether an applicant already knows
such things, for example by looking at https://wiki.debian.org/DDPortfolio
or minechangelogs https://lists.debian.org/debian-newmaint/2009/08/msg00002.html

But in those cases where there is not much visible activity, yet there are
reasons not to tell the applicant to gather some more experience then come
back, one can find these questions useful.


Bug Tracking System
-------------------

BT3. How (and when) does a package get from its first upload to a stable
     release? Can bug tags affect the release status of a debian package? Are
     there restrictions on who can/should set some tags?

BT5. What would you do if a bug was reported against your package and
     you are not able to fix it yourself?

BT7. Where can you find the list of defined BTS pseudo packages?
     Explain to me what a pseudo package is.


Procedures
----------

PR2. You've just heard about this great program and would like to package
     it, what are your next steps?

PR5. What are Non-Maintainer Uploads (NMUs) and when would you do an NMU?
     Please list the usual procedure for an NMU.

PR5a. What is a binNMU and what are they useful for? Where and how do
      you request to schedule binNMUs? Which precautions do you have to
      take to make your packages binNMU safe?

PRc. You decided adopt an orphaned package. What are your next steps?
